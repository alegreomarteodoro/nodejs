/**
 * Created by alegreomarteodoro on 18/07/2014.
 */
var app = angular.module('app', ['ngRoute', 'auth', 'user']);
var auth = angular.module('auth', ['ngCookies']);
var user = angular.module('user', []);

app.config(function($locationProvider, $routeProvider, USER_ROLES) {
    //$locationProvider.html5Mode(true);
    $routeProvider.
        when('/', {
            templateUrl: '/public/app/front/home/views/home.html',
            security : {
                authorizedRoles : [],
                onlyAuthenticated : false
            }
        }).
        when('/login', {
            templateUrl: '/public/app/front/auth/views/login.html',
            controller: 'LoginController',
            security : {
                authorizedRoles : [],
                onlyAuthenticated : false
            }
        }).
        when('/signup', {
            templateUrl: '/public/app/front/auth/views/signup.html',
            controller: 'SignupController',
            security : {
                authorizedRoles : [],
                onlyAuthenticated : false
            }
        }).
        when('/profile', {
            templateUrl: '/public/app/front/user/views/profile.html',
            controller: 'ProfileController',
            security : {
                authorizedRoles : [USER_ROLES.admin, USER_ROLES.normal],
                onlyAuthenticated : true
            }
        }).
        when('/users', {
            templateUrl: '/public/app/front/user/views/list.html',
            controller: 'UserListController',
            security : {
                authorizedRoles : [],
                onlyAuthenticated : false
            }
        }).
        when('/not-authorized', {
            templateUrl: '/public/app/front/error/views/not-authorized.html',
            security : {
                authorizedRoles : [],
                onlyAuthenticated : false
            }
        }).
        otherwise({
            redirectTo: '/'
        });
});


app.controller('AppController', function($scope, $rootScope, $location, AuthService, SessionService, AUTH_EVENTS){

    $scope.isAuthorized = AuthService.isAuthorized;
    $scope.getCurrentUser = SessionService.getCurrentUser;

    $scope.logout = function() {
        AuthService.logout()
            .success(function () {
                $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
            })
            .error(function (data, status, headers, config) {
                $rootScope.$broadcast(AUTH_EVENTS.logoutFailed);
            });
    };

    $scope.$on(AUTH_EVENTS.loginSuccess, function(event){
        console.log(event.name);
        $location.path('/profile');
    });

    $scope.$on(AUTH_EVENTS.signupSuccess, function(event){
        console.log(event.name);
        $location.path('/profile');
    });

    $scope.$on(AUTH_EVENTS.loginFailed, function(event){
        console.log(event.name);
        $location.path('/login');
    });

    $scope.$on(AUTH_EVENTS.logoutSuccess, function(event){
        console.log(event.name);
        $location.path('/');
    });

    $scope.$on(AUTH_EVENTS.logoutFailed, function(event){
        console.log(event.name);
    });

    $scope.$on(AUTH_EVENTS.notAuthenticated, function(event){
        console.log(event.name);
        $location.path('/login');
    });

    $scope.$on(AUTH_EVENTS.notAuthorized, function(event){
        console.log(event.name);
        $location.path('/not-authorized');
    });
});


app.run(function ($rootScope, AUTH_EVENTS, AuthService) {
    $rootScope.$on('$routeChangeStart', function (event, next) {
        var authorizedRoles = (next.security != undefined ? next.security.authorizedRoles : []);
        var onlyAuthenticated = (next.security != undefined ? next.security.onlyAuthenticated : false);

        if (!AuthService.isAuthenticated() && onlyAuthenticated) {
            event.preventDefault();
            // user is not logged in
            $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
        } else if (!AuthService.isAuthorized(authorizedRoles) && onlyAuthenticated) {
                event.preventDefault();
                // user is not allowed
                $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
        }
    });
});