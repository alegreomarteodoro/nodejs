/**
 * Created by alegreomarteodoro on 22/05/14.
 */

angular.module('auth').constant('USER_ROLES', {
    admin: 'admin',
    normal: 'normal',
    guest: 'guest',
    default: 'default'
});

angular.module('auth').constant('AUTH_EVENTS', {
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    logoutFailed: 'auth-logout-failed',
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized',
    signupSuccess: 'auth-signup-success',
    signupFailed: 'auth-signup-failed'
});

angular.module('auth').service('AuthService', function ($http, $window, $timeout, $rootScope, AUTH_EVENTS, SessionService) {

    var self = this;
    var urlBase = '/api/v1';

    self.localLogin = function (credentials) {
        var request = $http.post(urlBase + '/login', credentials);
        request.then(function(res){
            SessionService.create(res.data);
        });
        return request;
    };

    self.localSignup = function (credentials) {
        var request = $http.post(urlBase + '/signup', credentials);
        request.then(function(res){
            SessionService.create(res.data);
        });
        return request;
    };

    self.facebookLogin = function () {
        var width = 1000,
            height = 650,
            top = (window.outerHeight - height) / 2,
            left = (window.outerWidth - width) / 2;
        var facebookWindow = $window.open(urlBase + '/auth/facebook', 'Facebook Login', 'width=' + width + ',height=' + height + ',scrollbars=0,top=' + top + ',left=' + left);

        var checkUrl = function() {
            try {
                if (facebookWindow.location.pathname != urlBase + '/auth/facebook/callback') {
                    $timeout(checkUrl, 50);
                } else {
                    var userString = facebookWindow.document.body.innerHTML.replace(/(<([^>]+)>)/ig, "");
                    if (userString.length > 0) {
                        var user = angular.fromJson(userString);
                        if (user != undefined) {
                            SessionService.create(user);
                            facebookWindow.close();
                            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                        } else {
                            $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                        }
                    } else {
                        $timeout(checkUrl, 50);
                    }
                }
            } catch (error) {
                $timeout(checkUrl, 50);
            }
        };
        $timeout(checkUrl, 50);
    };

    self.twitterLogin = function () {
        var width = 1000,
            height = 650,
            top = (window.outerHeight - height) / 2,
            left = (window.outerWidth - width) / 2;
        var twitterWindow = $window.open(urlBase + '/auth/twitter', 'Twitter Login', 'width=' + width + ',height=' + height + ',scrollbars=0,top=' + top + ',left=' + left);

        var checkUrl = function() {
            try {
                if (twitterWindow.location.pathname != urlBase + '/auth/twitter/callback') {
                    $timeout(checkUrl, 50);
                } else {
                    var userString = twitterWindow.document.body.innerHTML.replace(/(<([^>]+)>)/ig, "");
                    if (userString.length > 0) {
                        var user = angular.fromJson(userString);
                        if (user != undefined) {
                            SessionService.create(user);
                            twitterWindow.close();
                            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                        } else {
                            $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                        }
                    } else {
                        $timeout(checkUrl, 50);
                    }
                }
            } catch (error) {
                $timeout(checkUrl, 50);
            }
        };
        $timeout(checkUrl, 50);
    };

    self.googleLogin = function () {
        var width = 1000,
            height = 650,
            top = (window.outerHeight - height) / 2,
            left = (window.outerWidth - width) / 2;
        var googleWindow = $window.open(urlBase + '/auth/google', 'Google Login', 'width=' + width + ',height=' + height + ',scrollbars=0,top=' + top + ',left=' + left);

        var checkUrl = function() {
            try {
                if (googleWindow.location.pathname != urlBase + '/auth/google/callback') {
                    $timeout(checkUrl, 50);
                } else {
                    var userString = googleWindow.document.body.innerHTML.replace(/(<([^>]+)>)/ig, "");
                    if (userString.length > 0) {
                        var user = angular.fromJson(userString);
                        if (user != undefined) {
                            SessionService.create(user);
                            googleWindow.close();
                            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                        } else {
                            $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                        }
                    } else {
                        $timeout(checkUrl, 50);
                    }
                }
            } catch (error) {
                $timeout(checkUrl, 50);
            }
        };
        $timeout(checkUrl, 50);
    };

    self.logout = function () {
        SessionService.destroy();
        var request = $http.get(urlBase + '/logout');
        return request;
    };

    self.isAuthenticated = function () {
        return !!SessionService.getCurrentUserId();
    };

    self.isAuthorized = function (authorizedRoles) {
        if (!angular.isArray(authorizedRoles)) {
            authorizedRoles = [authorizedRoles];
        }
        return (self.isAuthenticated() && authorizedRoles.indexOf(SessionService.getCurrentUserRole()) !== -1);
    };
});


angular.module('auth').service('SessionService', function ($cookieStore, USER_ROLES) {

    var self = this;

    self.create = function (currentUser) {
        $cookieStore.put('currentUserId', currentUser._id);
        $cookieStore.put('currentUserRole', (currentUser.role.admin == true ? USER_ROLES.admin : USER_ROLES.normal));
        $cookieStore.put('currentUser', currentUser);
    };

    self.destroy = function () {
        $cookieStore.remove('currentUserId');
        $cookieStore.remove('currentUserRole');
        $cookieStore.remove('currentUser');
    };

    self.getCurrentUserId = function () {
        return $cookieStore.get('currentUserId');
    };

    self.getCurrentUserRole = function () {
        return $cookieStore.get('currentUserRole');
    };

    self.getCurrentUser = function () {
        return $cookieStore.get('currentUser');
    };

    return self;
});