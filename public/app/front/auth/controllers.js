/**
 * Created by alegreomarteodoro on 18/07/2014.
 */

angular.module('auth').controller('LoginController', function ($scope, $rootScope, AuthService, AUTH_EVENTS) {

    $scope.credentials = {
        username : '',
        password : ''
    };

    $scope.localLogin = function (valid) {

        if (valid) {
            AuthService.localLogin($scope.credentials)
                .success(function (user) {
                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                })
                .error(function (data, status, headers, config) {
                    $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                });
        }
    };

    $scope.facebookLogin = function () {
        AuthService.facebookLogin();
    };

    $scope.twitterLogin = function () {
        AuthService.twitterLogin();
    };

    $scope.googleLogin = function () {
        AuthService.googleLogin();
    };

    $scope.logout = function () {
        AuthService.logout()
            .success(function () {
                $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
            })
            .error(function (data, status, headers, config) {
                $rootScope.$broadcast(AUTH_EVENTS.logoutFailed);
            });
    };

    $scope.getAvatar = function(){

    };
});

angular.module('auth').controller('SignupController', function ($scope, $rootScope, AuthService, AUTH_EVENTS) {

    $scope.credentials = {
        email : '',
        username : '',
        password : '',
        repeatPassword : ''
    };

    $scope.localSignup = function (valid) {

        if (valid) {
            AuthService.localSignup($scope.credentials)
                .success(function (user) {
                    $rootScope.$broadcast(AUTH_EVENTS.signupSuccess);
                })
                .error(function (data, status, headers, config) {
                    $rootScope.$broadcast(AUTH_EVENTS.signupFailed);
                });
        }
    };

});