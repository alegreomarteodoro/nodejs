/**
 * Created by alegreomarteodoro on 22/05/14.
 */

angular.module('user').service('UserService', function ($http, $q) {

    var self = this;
    var uid = 0;
    var urlBase = '/api/v1/users.json';

    self.save = function (user) {
        if(user._id)
            return $http.put(urlBase + '/' + user._id, user);
        return $http.post(urlBase, user);
    }

    self.find = function(q){
        return $http.get(urlBase + '/find/' + q)
    }

    self.get = function (id) {
        return $http.get(urlBase + '/' + id);
    }

    self.delete = function (id) {
        return $http.delete(urlBase + '/' + id);
    }

    self.deleteMultiple = function(ids){
        if(angular.isArray(ids)){
            var requests = [];
            angular.forEach(ids, function(userId){
                requests.push(self.delete(userId));
            });
            return $q.all(requests);
        }
    }

    self.disabled = function (id) {
        return $http.post(urlBase + '/' + id + '/disabled');
    }

    self.disabledMultiple = function(ids){
        if(angular.isArray(ids)){
            var requests = [];
            angular.forEach(ids, function(userId){
                requests.push(self.disabled(userId));
            });
            return $q.all(requests);
        }
    }

    self.enabled = function (id) {
        return $http.post(urlBase + '/' + id + '/enabled');
    }

    self.enabledMultiple = function(ids){
        if(angular.isArray(ids)){
            var requests = [];
            angular.forEach(ids, function(userId){
                requests.push(self.enabled(userId));
            });
            return $q.all(requests);
        }
    }

    self.setAsAdmin = function (id) {
        return $http.post(urlBase + '/' + id + '/set-as-admin');
    }

    self.setAsAdminMultiple = function (ids) {
        if(angular.isArray(ids)){
            var requests = [];
            angular.forEach(ids, function(userId){
                requests.push(self.setAsAdmin(userId));
            });
            return $q.all(requests);
        }
    }

    self.setAsNormal = function (id) {
        return $http.post(urlBase + '/' + id + '/set-as-normal');
    }

    self.setAsNormalMultiple = function (ids) {
        if(angular.isArray(ids)){
            var requests = [];
            angular.forEach(ids, function(userId){
                requests.push(self.setAsNormal(userId));
            });
            return $q.all(requests);
        }
    }

    self.getAll = function () {
        return $http.get(urlBase);
    }
});