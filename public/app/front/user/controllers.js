/**
 * Created by alegreomarteodoro on 18/07/2014.
 */

angular.module('user').controller('ProfileController', function ($scope) {


});


angular.module('user').controller('UserListController', function ($scope, $filter, UserService) {

    $scope.users = [],
        $scope.filteredUsers = [],
        $scope.currentFilter = null,
        $scope.predicate = 'createdAt',
        $scope.reverse = true,
        $scope.currentPage = 1,
        $scope.itemsPerPage = 20;
    $scope.maxSize = 5;
    $scope.listVisible = true;
    $scope.gridVisible = false;
    $scope.classItemList = 'row';

    $scope.getUsers = function(){
        UserService.getAll().success(function (data) {
            $scope.users = data;
            $scope.filteredUsers = $filter("filter")($scope.users, $scope.currentFilter);

            $scope.enabledUsers = $filter("filter")($scope.users, { active : true });
            $scope.adminUsers = $filter("filter")($scope.users, { role : { admin : true } });
            $scope.normalUsers = $filter("filter")($scope.users, { role : { admin : false } });
            $scope.disabledUsers = $filter("filter")($scope.users, { active : false });

            angular.element('.filter_all').addClass('active');
            angular.element('.bulk-actions').hide();
            angular.element('.items-list .headers .hidden-xs').show();
            angular.element('.items-list .headers input:checkbox').prop('checked',false);
        })
            .error(function (data, status, headers, config) {
                $scope.error = data;
                $scope.code = status;
                $scope.context = 'Module: user, Controllers: UserListController';
                $scope.template.url = '/javascripts/app/backend/default/views/error.html';
            });
    };
    $scope.getUsers();

    $scope.checkAll = function() {
        angular.forEach($scope.filteredUsers, function(user){
            user.selected = true;
        });

        if ($scope.change) {
            $scope.change();
        }
    };

    $scope.uncheckAll = function(){
        angular.forEach($scope.filteredUsers, function(user){
            user.selected = false;
        });

        if ($scope.change) {
            $scope.change();
        }
    };

    $scope.masterCheck = function(){
        if( angular.element('.items-list .headers input:checkbox').is(':checked') ){
            $scope.checkAll();
        }else{
            $scope.uncheckAll();
        }
    };

    $scope.getCheckedUsers = function(){
        return $filter("filter")($scope.filteredUsers, { selected : true });
    };

    $scope.change = function() {

        var lengthCheckedUsers = $scope.getCheckedUsers().length;
        if( lengthCheckedUsers > 0 ) {
            angular.element('.bulk-actions').show();
            angular.element('.bulk-actions .total-checked').html(' (' + lengthCheckedUsers + ' total users)');
            angular.element('.items-list .headers .hidden-xs').hide();
        } else {
            angular.element('.bulk-actions').hide();
            angular.element('.items-list .headers .hidden-xs').show();
            if( angular.element('.items-list .headers input:checkbox').is(':checked') ){
                angular.element('.items-list .headers input:checkbox').prop('checked',false);
            }
        }
    };

    $scope.filter = function(data, event){
        $scope.uncheckAll();

        angular.element('.filters .links a').removeClass('active');
        angular.element(event.target).addClass('active');

        $scope.currentFilter = data;
        $scope.filteredUsers = $filter("filter")($scope.users, data);
    }

    $scope.setPage = function(currentPage) {
        $scope.currentPage = currentPage;
    }

    $scope.pageChanged = function(scope) {
        //console.log('Page changed to: ' + scope.currentPage);
    };

    $scope.sortBy = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };

    $scope.$watch('currentPage + itemsPerPage', function() {
        var begin = (($scope.currentPage - 1) * $scope.itemsPerPage)
            , end = begin + $scope.itemsPerPage;

        $scope.filteredUsers = $scope.users.slice(begin, end);
    });

    $scope.$watch('currentFilter', function(){
        var paginationScope = angular.element('.pagination').scope();
        if(paginationScope)
            paginationScope.currentPage = 1;
    });

    $scope.disabledMultiple = function(){
        var userIds = [];
        angular.forEach($scope.getCheckedUsers(), function(user){
            userIds.push(user._id);
        });

        UserService.disabledMultiple(userIds).then(function(data){
            $scope.getUsers();
        });
    };

    $scope.enabledMultiple = function(){
        var userIds = [];
        angular.forEach($scope.getCheckedUsers(), function(user){
            userIds.push(user._id);
        });

        UserService.enabledMultiple(userIds).then(function(data){
            $scope.getUsers();
        });
    };

    $scope.setAsAdmin = function(){
        bootbox.dialog({
            message: 'Really want to set as administrator?',
            title: 'Please confirm the following',
            buttons:{
                cancel:{
                    label : "Cancel",
                    className : "btn-default",
                    callback: function() {
                        return;
                    }
                },
                confirm:{
                    label : "Confirm",
                    className : "btn-success",
                    callback: function() {
                        var userIds = [];
                        angular.forEach($scope.getCheckedUsers(), function(user){
                            userIds.push(user._id);
                        });

                        UserService.setAsAdminMultiple(userIds).then(function(data){
                            $scope.getUsers();
                        });
                    }
                }
            }
        });
    };

    $scope.setAsNormal = function(){
        var userIds = [];
        angular.forEach($scope.getCheckedUsers(), function(user){
            userIds.push(user._id);
        });

        UserService.setAsNormalMultiple(userIds).then(function(data){
            $scope.getUsers();
        });
    };

    $scope.deleteMultiple = function(){
        bootbox.dialog({
            message: 'Really want to permanently delete the selected users?',
            title: 'Please confirm the following',
            buttons:{
                cancel:{
                    label : "Cancel",
                    className : "btn-default",
                    callback: function() {
                        return;
                    }
                },
                confirm:{
                    label : "Confirm",
                    className : "btn-danger",
                    callback: function() {
                        var userIds = [];
                        angular.forEach($scope.getCheckedUsers(), function(user){
                            userIds.push(user._id);
                        });

                        UserService.deleteMultiple(userIds).then(function(data){
                            $scope.getUsers();
                        });
                    }
                }
            }
        });
    };

    $scope.showList = function(){
        angular.element('.show-options .list-view').addClass('active');
        angular.element('.show-options .grid-view').removeClass('active');
        angular.element('.items-list .headers').show();
        $scope.classItemList = 'row';
        $scope.listVisible = true;
        $scope.gridVisible = false;
    }

    $scope.showGrid = function(){
        angular.element('.show-options .grid-view').addClass('active');
        angular.element('.show-options .list-view').removeClass('active');
        angular.element('.items-list .headers').hide();
        $scope.classItemList = 'col-sm-3 col-xs-6';
        $scope.listVisible = false;
        $scope.gridVisible = true;
    }

});