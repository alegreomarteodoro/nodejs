/**
 * Created by alegreomarteodoro on 22/05/14.
 */
var app = angular.module('app', ['ngRoute', 'user', 'search']);
var user = angular.module('user', ['ui.bootstrap']);
var search = angular.module('search', ['user']);

app.config(function($locationProvider, $routeProvider) {
    //$locationProvider.html5Mode(true);
    $routeProvider.
        when('/users', {
            templateUrl: '/app/admin/user/views/list.html',
            controller: 'UserListController'
        }).
        when('/users/:userId', {
            templateUrl: '/app/admin/user/views/list.html',
            controller: 'UserListController'
        }).
        otherwise({
            redirectTo: '/users'
        });
});