/**
 * Created by alegreomarteodoro on 03/06/14.
 */

angular.module('search').controller('SearchController', function ($scope, UserService) {

    $scope.template = { url : '/javascripts/app/backend/search/views/search.html' };
    $scope.search = { q : ''};
    $scope.results = {
        users : []
    };

    $scope.go = function(){
        if($scope.search.q.length >= 3) {
            $scope.findUsers($scope.search.q);
            angular.element('li.search-results').removeClass('hidden');
            angular.element('li.search-results').addClass('open');
        } else {
            angular.element('li.search-results').addClass('hidden');
            angular.element('li.search-results').removeClass('open');
        }
    }

    $scope.findUsers = function(query){

        UserService.find(query).success(function(data){

            $scope.results.users = data;

        }).error(function (data, status) {

            $scope.error = data;
            $scope.code = status;
            $scope.context = 'Module: search, Controller: SearchController';
            $scope.template.url = '/javascripts/app/backend/default/views/error.html';

        });
    }
});