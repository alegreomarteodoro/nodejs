/**
 * Created by alegreomarteodoro on 22/05/14.
 */

angular.module('user').filter('startFrom', function() {
    return function(input, start) {
        return input.slice(start);
    }
});