/**
 * Created by alegreomarteodoro on 22/05/14.
 */

angular.module('user').directive('isAdmin', function() {
    return {
        restrict: 'E',
        scope: {
            value: '='
        },
        link: function(scope, elem, attrs) {
            if(scope.value) {
                elem.html('<span class="label label-info">admin</span>');
            } else {
                elem.html('<span class="label label-warning">normal</span>');
            }
        }
    };
});

angular.module('user').directive('status', function() {
    return {
        restrict: 'E',
        scope: {
            value: '='
        },
        link: function(scope, elem, attrs) {
            if(scope.value) {
                elem.html('<span class="label label-success">enabled</span>');
            } else {
                elem.html('<span class="label label-danger">disabled</span>');
            }
        }
    };
});

angular.module('user').directive('getAvatar', function() {
    return {
        restrict: 'E',
        scope: {
            value: '=',
            class: '='
        },
        link: function(scope, elem, attrs) {
            if(scope.value) {
                elem.html('<img src="' + scope.value + '" class="' + ( scope.class != undefined ? scope.class : 'img-circle' ) + '">');
            } else {
                elem.html('<img src="/images/avatar_2x.png" class="' + ( scope.class != undefined ? scope.class : 'img-circle' ) + '">');
            }
        }
    };
});