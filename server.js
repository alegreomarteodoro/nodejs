var express = require('express');
var http = require('http');
var app = express();
var autoloader = require('./core/libs/autoloader');

autoloader.environments('./config/environments', app);
autoloader.initializers('./config/initializers', app);
autoloader.routes('./app/routes', function(routes){
    require('./config/routes')(app, express, routes);
});

var ipaddress = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
http.createServer(app).listen(port, ipaddress, function(){
    console.log('Express server listening on port ' + port);
});