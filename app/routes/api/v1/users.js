/**
 * Created by OmarTeodoro on 01/12/2014.
 */

module.exports = {

    index: function (req, res) {
        db.User
            .find()
            .exec(function (error, users) {
                if (error) return res.json(error);
                res.json(users);
            });
    },

    disabled: function (request, response) {
        db.User.findById(request.params.id, function (error, user) {
            if (error) return response.json(error);
            user.active = false;
            user.save(function (error, user) {
                if (error) return response.json(error);
                return response.json(user);
            });
        });
    },

    enabled: function (request, response) {
        db.User.findById(request.params.id, function (error, user) {
            if (error) return response.json(error);
            user.active = true;
            user.save(function (error, user) {
                if (error) return response.json(error);
                return response.json(user);
            });
        });
    },

    delete: function (request, response) {
        var userId = request.params.id;

        db.User.remove({'_id': userId}, function (error, user) {
            if (error) return response.json(error);
            response.json(user);
        });
    },

    setAsAdmin: function (request, response) {
        db.User.findById(request.params.id, function (error, user) {
            if (error) return response.json(error);
            user.role.admin = true;
            user.save(function (error, user) {
                if (error) return response.json(error);
                return response.json(user);
            });
        });
    },

    setAsNormal: function (request, response) {
        db.User.findById(request.params.id, function (error, user) {
            if (error) return response.json(error);
            user.role.admin = false;
            user.save(function (error, user) {
                if (error) return response.json(error);
                return response.json(user);
            });
        });
    }

};