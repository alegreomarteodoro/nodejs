/**
 * Created by alegreomarteodoro on 19/06/2014.
 */
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var UserSchema = new mongoose.Schema({
    provider : {
        facebook : {
            id : String,
            token : String,
            displayName : String,
            email : String,
            username : String,
            createdAt : Date
        },
        google : {
            id : String,
            token : String,
            displayName : String,
            email : String,
            username : String,
            createdAt : Date
        },
        twitter : {
            id : String,
            token : String,
            displayName : String,
            email : String,
            username : String,
            createdAt : Date
        },
        github : {
            id : String,
            token : String,
            displayName : String,
            email : String,
            username : String,
            createdAt : Date
        },
        local : {
            username : String,
            password : String,
            displayName : String,
            email : String,
            createdAt : Date
        }
    },
    role : {
        admin : { type : Boolean, default: false }
    },
    displayName : String,
    name : {
        familyName : String,
        givenName : String,
        middleName : String
    },
    emails : [ String ],
    avatar: String,
    createdAt : { type: Date, default: Date.now },
    lastModified : { type: Date, default: Date.now },
    dateOfBirth : Date,
    phones : [{
        value : String,
        type : String
    }],
    active : { type: Boolean, default: true }
});

// generating a hash
UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// password verification
UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.provider.local.password);
};

UserSchema.methods.isAdmin = function(){
    return this.role.admin;
}

module.exports = UserSchema;
