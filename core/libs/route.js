var object = require('./object');
var fs = require('fs');
var path = require('path');

module.exports = {
    resolve : function (filename) {
        var self = this;
        var stats = fs.lstatSync(filename),
            route = {};

        if (stats.isDirectory()) {
            var temp = {};
            var dirs = fs.readdirSync(filename).map(function(child) {
                return self.resolve(filename + '/' + child);
            });
            dirs.forEach(function(obj){
                temp = object.extend({}, temp, obj);
            });

            route[path.basename(filename)] = temp;
        } else {
            route[path.basename(filename).replace('.js', '')] = require(path.resolve(filename.replace('.js', '')));
            route['path'] = filename;
        }

        return route;
    }
}