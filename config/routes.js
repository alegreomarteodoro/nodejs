
var passport = require('passport');

module.exports = function (app, express, routes) {

    /***********************
     * Admin Home
     ***********************/
    app.get('/admin', routes.back.home.index);



    /************************
     * Home
     ***********************/
    app.get('/', routes.front.home.index);



    /************************
     * API V1
     ***********************/
    var api_v1 = express.Router();

    api_v1.get('/', routes.api.v1.home.index);

    api_v1.post('/signup', passport.authenticate('local-signup', {}), function(req, res) {
        res.json(req.user);
    });

    api_v1.post('/login', passport.authenticate('local-login', {}), function(req, res) {
        res.json(req.user);
    });

    api_v1.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));
    api_v1.get('/auth/facebook/callback', passport.authenticate('facebook'), function(req, res) {
        res.json(req.user);
    });

    api_v1.get('/auth/twitter', passport.authenticate('twitter', { scope : 'email' }));
    api_v1.get('/auth/twitter/callback', passport.authenticate('twitter'), function(req, res) {
        res.json(req.user);
    });

    api_v1.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));
    api_v1.get('/auth/google/callback', passport.authenticate('google'), function(req, res) {
        res.json(req.user);
    });

    api_v1.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    api_v1.get('/users.json', routes.api.v1.users.index);
    api_v1.post('/users.json/:id/enabled', routes.api.v1.users.enabled);
    api_v1.post('/users.json/:id/disabled', routes.api.v1.users.disabled);
    api_v1.post('/users.json/:id/set-as-normal', routes.api.v1.users.setAsNormal);
    api_v1.post('/users.json/:id/set-as-admin', routes.api.v1.users.setAsAdmin);
    api_v1.delete('/users.json/:id', routes.api.v1.users.delete);

    app.use('/api/v1', api_v1);
};