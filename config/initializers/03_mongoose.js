/**
 * Created by alegreomarteodoro on 18/06/2014.
 */
var mongoose = require('mongoose');
var schemas = {
    userSchema : require('../../app/schemas/user')
};

module.exports = function(app) {

    var db_name = 'nodejstwo';
    var mongodb_connection_string = 'mongodb://localhost/' + db_name;

    if(process.env.OPENSHIFT_MONGODB_DB_URL){
        mongodb_connection_string = process.env.OPENSHIFT_MONGODB_DB_URL + db_name;
    }

    mongoose.connect(mongodb_connection_string);

    global.db = {
        User : mongoose.model('user', schemas.userSchema)
    }
};