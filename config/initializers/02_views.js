module.exports = function(app) {
    app.engine('jade', require('jade').__express);
    app.set('views', __dirname + '/../../app/views');
    //app.set("view options", { layout: "layout/default.jade" });
    app.set('view engine', 'jade');
};
