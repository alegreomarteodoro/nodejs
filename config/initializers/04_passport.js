/**
 * Created by alegreomarteodoro on 21/06/2014.
 */
var passport    = require('passport');
var LocalStrategy    = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy  = require('passport-twitter').Strategy;
var GoogleStrategy   = require('passport-google-oauth').OAuth2Strategy;

module.exports = function(app) {

    passport.serializeUser(function(user, done) {
        done(null, user._id);
    });

    passport.deserializeUser(function(id, done) {
        db.User.findById(id, function (err, user) {
            done(err, user);
        });
    });

    // LOCAL LOGIN
    passport.use('local-login', new LocalStrategy({
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true
        },
        function(req, username, password, done) {
            if (username)
                username = username.toLowerCase();

            // asynchronous
            process.nextTick(function() {
                db.User.findOne({ 'provider.local.username' : username }, function(err, user) {

                    if (err)
                        return done(err);

                    if (!user)
                        return done(null, false, {'message': 'No user found.'});

                    if (!user.validPassword(password))
                        return done(null, false, {'message' : 'Oops! Wrong password.'});

                    if (!user.active)
                        return done(null, false, {'message' : 'Oops! The user was suspended.'});

                    return done(null, user);
                });
            });
        }));

    // LOCAL SIGNUP
    passport.use('local-signup', new LocalStrategy({
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true
        },
        function(req, username, password, done) {
            if (username)
                username = username.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching
            // asynchronous
            process.nextTick(function() {

                db.User.findOne({ 'provider.local.username' :  username }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        return done(null, false, {'message': 'That username is already taken.'});

                    } else {

                        var requestUser = req.body;

                        if(password != requestUser.repeatPassword)
                            return done(null,  false, {'message': 'Passwords do not match'});

                        if(password == '' || requestUser.repeatPassword == '')
                            return done(null,  false, {'message': 'Passwords are required'});

                        if (!req.user) {

                            var newUser = new db.User();
                            newUser.provider.local.password = newUser.generateHash(password);
                            newUser.provider.local.username = username;
                            newUser.provider.local.email = requestUser.email;
                            newUser.provider.local.createdAt = Date.now();
                            //newUser.displayName = requestUser.givenName + ' ' + requestUser.familyName;
                            newUser.emails.push(requestUser.email);

                            newUser.provider.local.displayName = requestUser.displayName;
                            newUser.displayName = requestUser.displayName;

                            newUser.save(function(err) {
                                if (err)
                                    throw err;

                                return done(null, newUser);
                            });

                        } else {

                            var user = req.user;
                            user.provider.local.email = requestUser.email;
                            user.provider.local.password = user.generateHash(password);
                            user.provider.local.username = username;
                            user.provider.local.email = requestUser.email;
                            //user.provider.local.displayName = requestUser.givenName + ' ' + requestUser.familyName;
                            user.provider.local.createdAt = Date.now();
                            if(user.displayName.length < 1)
                                user.displayName = requestUser.givenName + ' ' + requestUser.familyName;

                            user.provider.local.displayName = requestUser.displayName;
                            user.displayName = requestUser.displayName;

                            user.emails.addToSet(profile.emails[0].value);
                            user.markModified('emails');

                            user.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });

                        }
                    }
                });

            });
        }));


    // FACEBOOK
    passport.use(new FacebookStrategy({

            clientID        : app.get('facebook.clientID'),
            clientSecret    : app.get('facebook.clientSecret'),
            callbackURL     : app.get('facebook.callbackURL'),
            passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

        },
        function(req, token, refreshToken, profile, done) {
            // asynchronous
            process.nextTick(function() {
                db.User.findOne({ 'provider.facebook.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        if (!user.active)
                            return done(null, false, {'message': 'Oops! The user was suspended.'});

                        return done(null, user);

                    } else {

                        if (!req.user) {

                            var newUser = new db.User();

                            newUser.provider.facebook.id = profile.id;
                            newUser.provider.facebook.token = token;
                            newUser.provider.facebook.name  = profile.name;
                            newUser.provider.facebook.displayName = profile.name.givenName + ' ' + profile.name.familyName;
                            newUser.provider.facebook.email = profile.emails[0].value;
                            newUser.provider.facebook.createdAt = Date.now();
                            newUser.displayName = profile.name.givenName + ' ' + profile.name.familyName;
                            newUser.emails.push(profile.emails[0].value);

                            newUser.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, newUser);
                            });

                        } else {

                            var user = req.user;

                            user.provider.facebook.id = profile.id;
                            user.provider.facebook.token = token;
                            user.provider.facebook.name  = profile.name;
                            user.provider.facebook.displayName = profile.name.givenName + ' ' + profile.name.familyName;
                            user.provider.facebook.email = profile.emails[0].value;
                            user.provider.facebook.createdAt = Date.now();
                            if(!user.displayName || user.displayName.length < 1)
                                user.displayName = profile.name.givenName + ' ' + profile.name.familyName;

                            user.emails.addToSet(profile.emails[0].value);
                            user.markModified('emails');

                            user.save(function(err) {
                                if (err)
                                    throw err;

                                return done(null, user);
                            });

                        }
                    }
                });
            });
        }));

    // TWITTER
    passport.use(new TwitterStrategy({

            consumerKey     : app.get('twitter.clientID'),
            consumerSecret  : app.get('twitter.clientSecret'),
            callbackURL     : app.get('twitter.callbackURL'),
            passReqToCallback : true

        },
        function(req, token, tokenSecret, profile, done) {
            // asynchronous
            process.nextTick(function() {
                db.User.findOne({ 'provider.twitter.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        if (!user.active)
                            return done(null, false, {'message': 'Oops! The user was suspended.'});

                        return done(null, user);

                    } else {

                        if (!req.user) {

                            var newUser = new db.User();

                            newUser.provider.twitter.id = profile.id;
                            newUser.provider.twitter.token = token;
                            newUser.provider.twitter.displayName = profile.displayName;
                            newUser.provider.twitter.createdAt = Date.now();
                            newUser.displayName = profile.displayName;


                            newUser.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, newUser);
                            });

                        } else {

                            var user = req.user;

                            user.provider.twitter.id = profile.id;
                            user.provider.twitter.token = token;
                            user.provider.twitter.displayName = profile.displayName;
                            user.provider.twitter.createdAt = Date.now();
                            if(!user.displayName || user.displayName.length < 1)
                                user.displayName = profile.displayName;

                            user.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });

                        }
                    }
                });
            });
        }));

    // GOOGLE
    passport.use(new GoogleStrategy({

            clientID        : app.get('google.clientID'),
            clientSecret    : app.get('google.clientSecret'),
            callbackURL     : app.get('google.callbackURL'),
            passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

        },
        function(req, token, refreshToken, profile, done) {
            // asynchronous
            process.nextTick(function() {
                db.User.findOne({ 'provider.google.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        if (!user.active)
                            return done(null, false, {'message': 'Oops! The user was suspended.'});

                        return done(null, user);

                    } else {

                        if (!req.user) {

                            var newUser = new db.User();

                            newUser.provider.google.id = profile.id;
                            newUser.provider.google.token = token;
                            newUser.provider.google.displayName  = profile.displayName;
                            newUser.provider.google.createdAt = Date.now();
                            newUser.provider.google.email = profile.emails[0].value;
                            newUser.displayName  = profile.displayName;

                            newUser.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, newUser);
                            });

                        } else {

                            var user = req.user;

                            user.provider.google.id = profile.id;
                            user.provider.google.token = token;
                            user.provider.google.displayName  = profile.displayName;
                            user.provider.google.createdAt = Date.now();
                            user.provider.google.email = profile.emails[0].value;
                            if(!user.displayName || user.displayName.length < 1)
                                user.displayName  = profile.displayName;
                            user.emails.addToSet(profile.emails[0].value);

                            user.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });

                        }
                    }
                });
            });
        }));
}