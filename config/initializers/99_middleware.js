var express = require('express');
var poweredBy = require('connect-powered-by');
var path = require('path');
var passport = require('passport');
var bodyParser = require('body-parser');
var expressSession = require('express-session');
var cookieParser = require('cookie-parser');
var methodOverride = require('method-override');
var stylus = require('stylus');
var logger = require('morgan');

module.exports = function(app) {

    app.use(cookieParser());
    app.use(expressSession({secret:'q1w2e3r4t5y6u7i8o9q1w2e3r4t5y6u7i8o9'}));
    app.use(logger('dev'));
    app.use(bodyParser());
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(poweredBy('alegreomarteodoro@gmail.com'));
    app.use(stylus.middleware({
        src: path.resolve('./resources'),
        dest: path.resolve('./public')
    }));
    app.use("/public", express.static(path.resolve('./public')));
    app.use(methodOverride());

};